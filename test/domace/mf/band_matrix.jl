#include("../band_matrix.jl")

using Test
using NumMat

@testset "Size and length of band matrix" begin
    A = BandMatrix([[]], [], [[]])
    B = BandMatrix([[1]], [3,3], [[1]])
    C = BandMatrix([[]], [1], [[]])

    @testset "band matrix size" begin
        @test size(A) == (0,0)  
        @test size(B) == (2,2)  
        @test size(C) == (1,1)  
    end

    @testset "band matrix length" begin
        @test length(A) == 0  
        @test length(B) == 2  
        @test length(C) == 100  
    end
end
