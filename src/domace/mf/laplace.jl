using NumMat
using Plots

function solve(border_problem; nx = 20, ny = 20)
    (a, b), (c, d) = border_problem.meje
    Z = zeros(nx + 2, ny + 2)
    x = LinRange(a, b, nx + 2)
    y = LinRange(c, d, ny + 2)
    Z[:, 1] = border_problem.rp[1].(x)
    Z[end, :] = border_problem.rp[2].(y)
    Z[:, end] = border_problem.rp[3].(x)
    Z[1, :] = border_problem.rp[4].(y)
    b = desne_strani(Z[2:end-1, 1], Z[end, 2:end-1],
                     Z[2:end-1, end], Z[1, 2:end-1], border_problem.operator)
    # TODO(miha): Here we call our function that returns BandMatrix struct
    L = matrika(nx, ny, border_problem.operator)

    # NOTE(miha): Build a band matrix, so we can solve the system.
    lower = Vector{Vector{Float64}}()
    for i=1:nx+1
        push!(lower, get_diagonal(L, i))
    end

    upper = Vector{Vector{Float64}}()
    for i=-1:-1:-nx-1
        push!(upper, get_diagonal(L, i))
    end

    B = BandMatrix(upper, get_diagonal(L, 0), lower)

    # NOTE(miha): Solve the system
    Z[2:end-1, 2:end-1] = reshape(L\b, nx, ny)
    #println("--- B\b ---")
    #r1 = B\b
    #println(r1)
    #println("--- L\b ---")
    #r2 = L\b
    #println(r2)
    #println("----------")
    #println(r1≈r2)

    return Z',x,y, b
end

function benchmark(border_problem)
    Z, x, y, right_side_values = solve(border_problem; nx=20, ny=20)
end

L2 = LaplaceovOperator(2)

fs(x) = x
fdd(y) = y+1
fz(x) = x+2
fl(y) = y
h = 0.1
border = ((0, 1), (0, 2))
border_problem = RobniProblemPravokotnik(
    L2,
    ((0, pi), (0, pi)),
    [sin, y->0, sin, y->0] 
)

#; nx=3, ny=3
Z, x, y, right_side_values = solve(border_problem; nx=20, ny=20)
wireframe(x, y, Z)
#savefig("milnica.png")
