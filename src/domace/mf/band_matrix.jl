# NOTE(miha): Used for identiy matrix (LinearAlgebra.I).
import LinearAlgebra

# NOTE(miha): Used for "prettier" output for band matrices.
using Printf

"""
    B = BandMatrix(upper, diagonal, lower)

Data structure for band matrix. `upper` and `lower` are two-dimensional arrays
which contains elements on the band. `diagonal` is a one-dimensional array that
contains the lement on the diagonal.

# Examples
```julia-repl
julia> B = BandMatrix([[3,3,3],[2,2]], [11,11,11,11], [[3,3,3], [2,2]])
11.00,   3.00,   2.00,   0.00,
 3.00,  11.00,   3.00,   2.00,
 2.00,   3.00,  11.00,   3.00,
 0.00,   2.00,   3.00,  11.00,
```
"""
struct BandMatrix
    upper::Vector{Vector{Float64}}
    diagonal::Vector{Float64}
    lower::Vector{Vector{Float64}}
end

"""
    U = UpperBandMatrix(upper, diagonal)

Data structure for upper band matrix. `upper` is a two-dimensional array which
contains elements on the band. `diagonal` is a one-dimensional array which
contains elements of the diagonal.

# Examples
```julia-repl
julia> U = UpperBandMatrix([[3,3,3], [2,2]], [11,11,11,11])
11.00,   3.00,   2.00,   0.00,
 0.00,  11.00,   3.00,   2.00,
 0.00,   0.00,  11.00,   3.00,
 0.00,   0.00,   0.00,  11.00,
```
"""
struct UpperBandMatrix
    upper::Vector{Vector{Float64}}
    diagonal::Vector{Float64}
end

"""
    L = LowerBandMatrix(diagonal, lower)

Data structure for lower band matrix. `lower` is a two-dimensional array which
contains elements on the band. `diagonal` is a one-dimensional array which
contains elements of the diagonal.

# Examples
```julia-repl
julia> L = LowerBandMatrix([11,11,11,11], [[3,3,3], [2,2]])
11.00,   0.00,   0.00,   0.00,
 3.00,  11.00,   0.00,   0.00,
 2.00,   3.00,  11.00,   0.00,
 0.00,   2.00,   3.00,  11.00,
```
"""
struct LowerBandMatrix
    diagonal::Vector{Float64}
    lower::Vector{Vector{Float64}}
end

"""
    size(B::BandMatrix)

Return size (rows, cols) of band matrix.

# Examples
```julia-repl
julia> B = BandMatrix([[3,3,3],[2,2]], [11,11,11,11], [[3,3,3], [2,2]])
julia> rows, cols = size(B)
(4, 4)
```
"""
function Base.size(B::BandMatrix)
    n = length(B.diagonal)
    return (n, n)
end

"""
    length(B::BandMatrix)

Return length of band matrix.
"""
function Base.length(B::BandMatrix)
    return length(B.diagonal)
end

"""
    length(U::UpperBandMatrix)

Return length of upper band matrix.
"""
function Base.length(U::UpperBandMatrix)
    return length(U.diagonal)
end

"""
    length(L::LowerBandMatrix)

Return length of lower band matrix.
"""
function Base.length(L::LowerBandMatrix)
    return length(L.diagonal)
end

"""
    a = copy(B::BandMatrix)

Copies band matrix `B` to the new band matrix `a`.
"""
function Base.copy(B::BandMatrix)
    return BandMatrix(copy(B.upper), copy(B.diagonal), copy(B.lower))
end

"""
    a = copy(B::BandMatrix)

Deep copies band matrix `B` to the new band matrix `a`.
"""
function Base.deepcopy(B::BandMatrix)
    return BandMatrix(deepcopy(B.upper), deepcopy(B.diagonal), deepcopy(B.lower))
end

"""
    print_band_matrix(B::BandMatrix)

Print given band matrix `B`.

Printed values are rounded to two decimal places.
"""
function print_band_matrix(B::BandMatrix)
    rows, cols = size(B)
    for i=1:cols
        for j=1:rows
            @printf("%6.2f, ", B[i,j])
        end
        println()
    end
end

"""
    print_vector(v::Vector)

Print given vector `v`.
"""
function print_vector(v::Vector)
    for i=1:length(v)
        println(v[i])
    end
end

"""
    print_matrix(M::Matrix)

Print given matrix `M`.
"""
function print_matrix(M::Matrix)
    for i=1:size(M)[1]
        for j=1:size(M)[2]
            @printf("%6.2f, ", M[i,j])
        end
        println()
    end
end

"""
    element = B[i,j]

Return element in band matrix `B` at `i`-th row and `j`-th column. 

If the index is out of bounds return `OutOfBoundsError`. If the element under
the index is not on the matrix band, return 0.
"""
function Base.getindex(B::BandMatrix, I...)
    row, column = I
    len = length(B)

    if row > len || column > len
        error("OutOfBoundsError, element [$(row), $(column)] is out of matrix bounds (dimension: [$(length), $(length)].\n")
    end

    # NOTE(miha): Return element in the diagonal part of the band matrix.
    if row == column
        return B.diagonal[row]
    end

    # NOTE(miha): Return element in the upper part of the band matrix.
    if row < column
        try
            r = column - row
            c = column - r
            return B.upper[r][c]
        catch err
            # NOTE(miha): If we try to access element that is not on the band,
            # but it is inside the matrix return 0 (as per band matrix
            # definition).
            if isa(err, BoundsError)
                return 0
            end
        end
    end

    # NOTE(miha): Return element in the lower part of the band matrix.
    if row > column
        try
            r = row - column
            c = column
            return B.lower[r][c]
        catch err
            # NOTE(miha): If we try to access element that is not on the band,
            # but it is inside the matrix return 0 (as per band matrix
            # definition).
            if isa(err, BoundsError)
                return 0
            end
        end
    end
end

"""
    element = U[i,j]

Return element in upper band matrix `U` at `i`-th row and `j`-th column. 

If the index is out of bounds return `OutOfBoundsError`. If the element under
the index is not on the matrix band, return 0.
"""
function Base.getindex(U::UpperBandMatrix, I...)
    row, column = I
    len = length(U)

    if row > len || column > len
        error("OutOfBoundsError, element [$(row), $(column)] is out of matrix bounds (dimension: [$(length), $(length)].\n")
    end

    # NOTE(miha): Return element in the diagonal part of the band matrix.
    if row == column
        return U.diagonal[row]
    end

    # NOTE(miha): Return element in the upper part of the band matrix.
    try
        r = column - row
        c = column - r
        return U.upper[r][c]
    catch err
        # NOTE(miha): If we try to access element that is not on the band,
        # but it is inside the matrix return 0 (as per band matrix
        # definition).
        if isa(err, BoundsError)
            return 0
        end
    end
end

"""
    element = L[i,j]

Return element in lower band matrix `L` at `i`-th row and `j`-th column. 

If the index is out of bounds return `OutOfBoundsError`. If the element under
the index is not on the matrix band, return 0.
"""
function Base.getindex(L::LowerBandMatrix, I...)
    row, column = I
    len = length(L)

    if row > len || column > len
        error("OutOfBoundsError, element [$(row), $(column)] is out of matrix bounds (dimension: [$(length), $(length)].\n")
    end

    # NOTE(miha): Return element in the diagonal part of the band matrix.
    if row == column
        return L.diagonal[row]
    end
    # NOTE(miha): Return element in the lower part of the band matrix.
    try
        r = row - column
        c = column
        return L.lower[r][c]
    catch err
        # NOTE(miha): If we try to access element that is not on the band,
        # but it is inside the matrix return 0 (as per band matrix
        # definition).
        if isa(err, BoundsError)
            return 0
        end
    end
end

"""
    B[i,j] = element

Set band matrix `B` at row `i` and column `j` to the value `element`.
"""
function Base.setindex!(B::BandMatrix, X, I...)
    row, column = I
    len = length(B)

    if row > len || column > len || row < 1 || column < 1
        error("Access to the element [$(row), $(column)] is out of bounds (matrix dimension: [$(len), $(len)]).\n")
    end

    # NOTE(miha): Set element at the diagonal part of the band matrix.
    if row == column 
        B.diagonal[row] = X
    end

    # NOTE(miha): Set element at the upper part of the band matrix.
    if row < column
        try
            r = column - row
            c = column - r
            B.upper[r][c] = X
        catch err
            println("errr: ", err)
            if isa(err, BoundsError)
                error("Cannot set non-band element [$(row), $(column)]")
            end
        end
    end

    # NOTE(miha): Set element at the lower part of the band matrix.
    if row > column
        try
            r = row - column
            c = column
            B.lower[r][c] = X
        catch err
            if isa(err, BoundsError)
                error("Cannot set non-band element [$(row), $(column)]")
            end
        end
    end
end

"""
    U[i,j] = element

Set upper band matrix `U` at row `i` and column `j` to the value `element`.
"""
function Base.setindex!(U::UpperBandMatrix, X, I...)
    row, column = I
    len = length(U)

    if row > len || column > len || row < 1 || column < 1
        error("Access to the element [$(row), $(column)] is out of bounds (matrix dimension: [$(len), $(len)]).\n")
    end

    # NOTE(miha): Set element at the diagonal part of the band matrix.
    if row == column 
        U.diagonal[row] = X
        return
    end

    # NOTE(miha): Set element at the upperr part of the band matrix.
    try
        r = column - row
        c = colum - r
        U.upper[r][c] = X
    catch err
        if isa(err, BoundsError)
            error("Cannot set non-band element [$(row), $(column)]")
        end
    end
end

"""
    L[i,j] = element

Set lower band matrix `L` at row `i` and column `j` to the value `element`.
"""
function Base.setindex!(L::LowerBandMatrix, X, I...)
    row, column = I
    len = length(L)

    if row > len || column > len || row < 1 || column < 1
        error("Access to the element [$(row), $(column)] is out of bounds (matrix dimension: [$(len), $(len)]).\n")
    end

    # NOTE(miha): Set element at the diagonal part of the band matrix.
    if row == column 
        L.diagonal[row] = X
        return
    end

    # NOTE(miha): Set element at the lower part of the band matrix.
    try
        r = row - column
        c = column
        L.lower[r][c] = X
    catch err
        if isa(err, BoundsError)
            error("Cannot set non-band element [$(row), $(column)]")
        end
    end
end

"""
    result = B*v

Band matrix `B` multiplication with vector `v` is saved in vector `result`. 
"""
function Base.:*(B::BandMatrix, v::Vector)
    len = length(B)
    band_size = size(B.upper)[1]
    result = zeros(len)

    for i=1:len
        for j=i-band_size:i+band_size
            if j < 1 || j > len
                continue
            end

            result[i] += B[i, j] * v[j]
        end
    end

    return result
end

"""
    c::BandMatrix = A::BandMatrix*B::BandMatrix

Multiplication between band matrix `A` and band matrix `B`, result is saved
into the band matrix `c`.
"""
function Base.:*(A::BandMatrix, B::BandMatrix)
    a = Matrix(deepcopy(A))
    b = Matrix(deepcopy(B))
    n = length(A)
    band_size = length(A.upper)

    c = a*b

    lower = Vector{Vector{Float64}}()
    for i=1:band_size
        push!(lower, get_diagonal(c, i))
    end

    upper = Vector{Vector{Float64}}()
    for i=-band_size:-1
        push!(upper, get_diagonal(c, i))
    end

    return BandMatrix(upper, get_diagonal(c, 0), lower)
end

"""
    b = get_band_size(B::BandMatix)

Returns size of band from band matrix `B` into an intger `b`.

# Examples
```julia-repl
julia> B = BandMatrix([[3,3,3],[2,2]], [11,11,11,11], [[3,3,3], [2,2]])
julia> band_size = get_band_size(B)
2
```
"""
function get_band_size(B::BandMatrix)
    return length(B.upper)
end

"""
    b = get_band_size(U::UpperBandMatix)

Returns size of band from upper band matrix `U` into an intger `b`.

# Examples
```julia-repl
julia> U = UpperBandMatrix([[3,3,3],[2,2]], [11,11,11,11])
julia> band_size = get_band_size(U)
2
```
"""
function get_band_size(U::UpperBandMatrix)
    return length(U.upper)
end

"""
    b = get_band_size(L::LowerBandMatix)

Returns size of band from lower band matrix `L` into an intger `b`.

# Examples
```julia-repl
julia> L = LowerBandMatrix([11,11,11,11], [[3,3,3], [2,2]])
julia> band_size = get_band_size(L)
2
```
"""
function get_band_size(L::LowerBandMatrix)
    return length(L.lower)
end

"""
    v = get_row(L::LowerBandMatrix, index)

Returns row from lower band matrix `L` at index `index` into vector `v`.
"""
function get_row(L::LowerBandMatrix, index)
    len = length(L.diagonal)
    result = zeros(len)

    for i=1:len
        result[i] = L[index, i]
    end

    return result'
end

"""
    v = get_column(U::UperBandMatrix, index)

Returns column from upper band matrix `U` at index `index` into vector `v`.
"""
function get_column(U::UpperBandMatrix, index)
    len = length(U.diagonal)
    result = zeros(len)

    for i=1:len
        result[i] = U[i, index]
    end

    return result
end

"""
    c::BandMatrix = A::LowerBandMatrix*B::UpperBandMatrix

Multiplication between lower band matrix `A` and upper band matrix `B`, result
is saved into the band matrix `c`.
"""
function Base.:*(L::LowerBandMatrix, U::UpperBandMatrix)
    if length(L.diagonal) != length(U.diagonal)
        error("Matrix L (dimension: ($(size(L)[1]), $(size(L)[2]))) is not the same dimension as matrix U (dimension: ($(size(U)[1]), $(size(U)[2]))).")
        return
    end

    len = length(L.diagonal)
    result = zeros((len, len))

    for i=1:len
        row = get_row(L, i)
        for j=1:len
            column = get_column(U, j)
            result[i,j] = row*column
        end
    end

    band_size = length(U.upper)

    lower = Vector{Vector{Float64}}()
    for i=1:band_size
        push!(lower, get_diagonal(result, i))
    end

    upper = Vector{Vector{Float64}}()
    for i=-1:-1:-band_size
        push!(upper, get_diagonal(result, i))
    end

    return BandMatrix(upper, get_diagonal(result, 0), lower)
end

"""
    B = BandMatrix(U::UpperBandMatrix)

Conver upper band matrix `U` into band matrix `B`.
"""
function BandMatrix(U::UpperBandMatrix)
    return BandMatrix(U.upper, U.diagonal, [[]])
end

"""
    B = BandMatrix(L::UpperBandMatrix)

Conver lower band matrix `L` into band matrix `B`.
"""
function BandMatrix(L::LowerBandMatrix)
    return BandMatrix([[]], L.diagonal, L.lower) 
end

"""
    M = Matrix(B::BandMatrix)

Convert band matrix `B` to matrix `M`::Matrix.
"""
function Base.Matrix(B::BandMatrix)
    len = length(B)
    A = zeros(Float64, len, len)
    for i=1:len
        for j=1:len
            A[i,j] = B[i,j]
        end
    end
    return A
end

"""
    M = Matrix(U::UpperBandMatrix)

Convert upper band matrix `U` to matrix `M`::Matrix.
"""
function Base.Matrix(U::UpperBandMatrix)
    len = length(U)
    A = zeros(Float64, len, len)
    for i=1:len
        for j=1:len
            A[i,j] = U[i,j]
        end
    end
    return A
end

"""
    M = Matrix(L::LowerBandBandMatrix)

Convert lower band matrix `L` to matrix `M`::Matrix.
"""
function Base.Matrix(L::LowerBandMatrix)
    len = length(L)
    A = zeros(Float64, len, len)
    for i=1:len
        for j=1:len
            A[i,j] = L[i,j]
        end
    end
    return A
end

"""
    is_strictly_diagonally_dominant(B::BandMatrix)

Check if band matrix `B` is strictly diagonally dominant (SDD). If the matrix
is not SDD, print out warning.
"""
function is_strictly_diagonally_dominant(B::BandMatrix)
    len = length(B)
    flag = false

    for i=1:len
        diagonal = 0.0
        others = 0.0
        for j=1:len
            if i == j
                diagonal = abs(B[i,j])
                continue
            end

            others += abs(B[i,j])
        end

        if !(diagonal > others)
            flag = true
        end
    end

    if flag
        @warn "Matrix $(B) is not strictly diagonally dominant"
    end
end

"""
    x = B/b

Solve linear equation Bx = b for band matrix `B`.
"""
function Base.:\(B::BandMatrix, b::Vector)
    A = deepcopy(B)
    x = deepcopy(b)
    result = ones(length(b))
    len = length(A)
    band_size = size(A.upper)[1]

    # NOTE(miha): Gauss elimination
    for i=2:len
        for j=1:band_size
            if i+j-1 > len
                break
            end

            a = A[i+j-1,i-1]
            mult = a/A[i-1,i-1]
            x[i+j-1] = x[i+j-1] - mult*x[i-1]
            for k=1:band_size+1
                if i+k-2 > len
                    break
                end

                A[i+j-1,i+k-2] -= mult*A[i-1,i+k-2]
            end
        end
    end

    # NOTE(miha): Back substitution
    for i=len:-1:1
        number = 0.0
        for j=i+1:i+band_size
            if j > len
                break
            end

            number += A[i, j]*result[j]
        end
        result[i] = (x[i] - number)/A.diagonal[i]
    end

    return result
end

"""
    L = zeros_lower_band_matrix(length, band_size)

Generates a lower band matrix with length `length` and band size `band_size`.

# Examples
```julia-repl
julia> L = zeros_lower_band_matrix(4, 2)
# X denote the elements not in the lower band matrix
# L contains zeros on the following positions: 
# 0.0   X    X    X
# 0.0  0.0   X    X
# 0.0  0.0  0.0   X
#  X   0.0  0.0  0.0
```
"""
function zeros_lower_band_matrix(length, band_size)
    len = length
    M = Vector{Vector{Float64}}()

    for i=1:band_size+1
        push!(M, [])
        for j=1:len
            push!(M[i], 0.0)
        end
        len -= 1
    end

    return LowerBandMatrix(copy(M[1]), copy(M[2:end]))
end

"""
    U = zeros_upper_band_matrix(length, band_size)

Generates an uppere band matrix with length `length` and band size `band_size`.

# Examples
```julia-repl
julia> U = zeros_upper_band_matrix(4, 2)
# X denote the elements not in the upper band matrix
# L contains zeros on the following positions: 
# 0.0  0.0  0.0   X
#  X   0.0  0.0  0.0
#  X    X   0.0  0.0
#  X    X    X   0.0
```
"""
function zeros_upper_band_matrix(length, band_size)
    len = length
    M = Vector{Vector{Float64}}()

    for i=1:band_size+1
        push!(M, [])
        for j=1:len
            push!(M[i], 0.0)
        end
        len -= 1
    end

    return UpperBandMatrix(M[2:end], M[1])
end

"""
    B = zeros_band_matrix(length, band_size)

Generates a band matrix with length `length` and band size `band_size`.

# Examples
```julia-repl
julia> B = zeros_band_matrix(4, 2)
# X denote the elements not in the lower band matrix
# L contains zeros on the following positions: 
# 0.0  0.0  0.0   X
# 0.0  0.0  0.0  0.0
# 0.0  0.0  0.0  0.0
#  X   0.0  0.0  0.0
```
"""
function zeros_band_matrix(length, band_size)

    upper = zeros_upper_band_matrix(length, band_size)
    lower = zeros_lower_band_matrix(length, band_size)

    return BandMatrix(upper.upper, upper.diagonal, lower.lower)
end

"""
    v = get_diagonal(M, relative)

Returns the diagonal from matrix `M`. `M` can be BandMatrix, Matrix
UpperBandMatrix or LowerBandMatrix. `relative` tells the offset from the main
diagonal.

# Examples
```julia-repl
julia> B = BandMatrix([[4,4,4],[3,3]], [11,11,11,11], [[2,2,2], [1,1]])
julia> v = get_diagonal(B, 0)
11.0
11.0
11.0
11.0
julia> v = get_diagonal(B, -1)
4.0
4.0
4.0
julia> v = get_diagonal(B, 1)
2.0
2.0
2.0
```
"""
function get_diagonal(M, relative)
    n = size(M)[1]
    v = Vector{Float64}()

    for i=1:n
        for j=1:n
            if i-j == relative
                push!(v, M[i,j])
            end
        end
    end

    return v
end

"""
    l,u = lu(B::BandMatrix)

LU factorization of the band matrix `B`. Returns lower band matrix `l` and
upper band matrix `u`. If the matrix `B` is not strictly diagonally dominant,
warning is printed.
"""
function lu(B::BandMatrix)
    is_strictly_diagonally_dominant(B)
    U = deepcopy(B)
    x = ones(length(B)[1])
    n = length(U)
    band_size = size(U.upper)[1]
    L = zeros_lower_band_matrix(n, band_size)

    # NOTE(miha): Gauss elimination
    for i=2:n
        for j=1:band_size
            if i+j-1 > n
                break
            end

            a = U[i+j-1,i-1]
            mult = a/U[i-1,i-1]
            x[i+j-1] = x[i+j-1] - mult*x[i-1]
            for k=1:band_size+1
                if i+k-2 > nm_1
                    break
                end

                U[i+j-1,i+k-2] -= mult*U[i-1,i+k-2]
            end
            L[i+j-1,i-1] = mult
        end
    end

    # NOTE(miha): Diagonal of the lower matrix is identity.
    for i=1:n
        L[i,i] = 1
    end

    U = UpperBandMatrix(copy(U.upper), copy(U.diagonal))
    
    return L,U
end
