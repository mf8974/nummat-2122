# Homework 1: Band matrices

## Band matrices
Because larger band matrices can have a lot of zero elements, there is no reason to save them in a matrix (requires $$O(n^2)$$ space, $$n$$ is the length of the main diagonal). Instead, we can use $$O(2b+1)$$ space, where $$b$$ is the bandwidth of a matrix. We defined the struct **BandMatrix** the following way:

```julia
struct BandMatrix
    upper::Vector{Vector{Float64}}
    diagonal::Vector{Float64}
    lower::Vector{Vector{Float64}}
end
```

This struct has two 2-dimensional arrays for storing the diagonals above and below the main diagonal. It also has an array for storing the main diagonal. For example, look at the below matrix and the way we store it in the struct.

$$\left(\begin{array}{} 
1 & 2 & 3 & 0 & 0 & 0 & 0\\
4 & 1 & 2 & 3 & 0 & 0 & 0\\
5 & 4 & 1 & 2 & 3 & 0 & 0\\
0 & 5 & 4 & 1 & 2 & 3 & 0\\
0 & 0 & 5 & 4 & 1 & 2 & 3\\
0 & 0 & 0 & 5 & 4 & 1 & 2\\
0 & 0 & 0 & 0 & 5 & 4 & 1
\end{array}\right)$$ 

```julia-repl
B = BandMatrix([[2,2,2,2,2,2],[3,3,3,3,3]], [1,1,1,1,1,1,1], [[4,4,4,4,4,4],[5,5,5,5,5]])
```

We first store upper diagonals (from the main diagonal upwards), then the main diagonal, and the last lower diagonals (from the main diagonal downwards). In the same fashion, we also created structs **UpperBandMatrix** and **LowerBandMatrix**, which just omit the upper or lower diagonals.

## Gaussian elimination - GE
Standard GE run time requires roughly $$O(n^3)$$, meanwhile, a special GE algorithm for band matrices has run time $$O(n)$$ (because bandwidth is constant). Like in standard GE, for GE algorithm for band matrices consists of two parts: elimination and back substitution. In the first part, we are eliminating elements below the main diagonal and respectively updating the values on the right side. In the second part, we are starting from the bottom of the matrix (where there is one known value) and traversing to the top subtitling already known values to compute the unknown values. When all unknown values are computed we found a solution for $$Ax=b$$, where x is unknown.

## LU factorization
The procedure is very similar to the GE algorithm for band matrices, we get the U matrix by running the algorithm, then we just need to save the steps into the L matrix.

## Soloving 2D Laplace equation
For example, we tried solving the 2D Laplace equation with our struct **BandMatrix**. We created the same problem as in the [Minimal Surfaces] example. We compared the results of our GE algorithm with Julia's build-in equation solving algorithm. Our GE algorithm was approximately equal (symbol ≈ in Julia) to Julia's in build one, we concluded that our GE algorithm correctly solves the equation.

Below is a picture of a resulting surface that was obtained using struct **BandMatrix** and our GE algorithm.

![Resulting saddle from solving 2D Laplace equation](./sedlo.png)

[//]: #
[Minimal Surfaces]: <https://nummat.gitlab.io/nummat-2122/vaje/2_linearni_sistemi/03_minimalne_ploskve>
