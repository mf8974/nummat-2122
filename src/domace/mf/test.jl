using Test
using NumMat
using LinearAlgebra

@testset "Size and length tests" begin
    A = BandMatrix([[]], [], [[]])
    B = BandMatrix([[1]], [3,3], [[1]])
    C = BandMatrix([ones(10), ones(9)], ones(11), [ones(10), ones(9)])
    a = copy(A)
    b = copy(B)
    c = copy(C)

    @testset "band matrix size" begin
        @test size(A) == (0,0)  
        @test size(B) == (2,2)  
        @test size(C) == (11,11)  
    end

    @testset "band matrix length" begin
        @test length(A) == 0  
        @test length(B) == 2  
        @test length(C) == 11 
    end

    U1 = UpperBandMatrix([[]], [])
    U2 = UpperBandMatrix([[1]], [3,3])
    U3 = UpperBandMatrix([ones(10), ones(9)], ones(11))
    @testset "upper band matrix length" begin
        @test length(U1) == 0
        @test length(U2) == 2
        @test length(U3) == 11
    end

    L1 = LowerBandMatrix([], [[]])
    L2 = LowerBandMatrix([3,3], [[1]])
    L3 = LowerBandMatrix(ones(11), [ones(10), ones(9)])
    @testset "lower band matrix length" begin
        @test length(L1) == 0
        @test length(L2) == 2
        @test length(L3) == 11
    end
end

@testset "getindex and setindex tests" begin
    A = BandMatrix([[]], [], [[]])
    B = BandMatrix([[1]], [3,3], [[1]])
    C = BandMatrix([ones(10), ones(9)], ones(11), [ones(10), ones(9)])

    @testset "band matrix getindex" begin
        @test B[1,1] == 3
        @test C[3,9] == 0
        @test C[9,3] == 0
    end

    @testset "band matrix setindex" begin
        b = copy(B)
        b[1,1] = 5
        @test b[1,1] == 5
    end

    U1 = UpperBandMatrix([[]], [])
    U2 = UpperBandMatrix([[1]], [3,3])
    U3 = UpperBandMatrix([ones(10), ones(9)], ones(11))
    @testset "upper band matrix getindex" begin
        @test U2[1,1] == 3
        @test U3[3,9] == 0
        @test U3[9,3] == 0
    end

    @testset "upper band matrix setindex" begin
        U2[1,1] = 5
        @test U2[1,1] == 5
    end

    L1 = LowerBandMatrix([], [[]])
    L2 = LowerBandMatrix([3,3], [[1]])
    L3 = LowerBandMatrix(ones(11), [ones(10), ones(9)])
    @testset "lower band matrix getindex" begin
        @test L2[1,1] == 3
        @test L3[3,9] == 0
        @test L3[9,3] == 0
    end

    @testset "lower band matrix setindex" begin
        L2[1,1] = 5
        @test L2[1,1] == 5
    end
end

@testset "multiplication and solving equations" begin
    I = BandMatrix([[0]], [1,1], [[0]])
    A = BandMatrix([[1.0]], [3.0,3.0], [[1.0]])

    @testset "multiplication" begin
        a = [7.0, 7.0]
        @test A*a == [28.0, 28.0]
        @test I*a == [7.0, 7.0]
    end

    @testset "solving equations" begin
        a = [3.0, 2.0]
        x = A\a
        @test A\a == [0.875, 0.375]
        @test A*x == a
        @test I\a == a
        @test I*a == a
        B = BandMatrix([[3.0, 9.0, 2.0]], [2.0, 3.0, 5.0, 3.0], [[6.0, 2.0, 4.0]])
        b = [21.0, 69.0, 34.0, 22.0]
        x = B\b
        @test B\b == [3.0, 5.0, 4.0, 2.0]
        @test B*x == b
    end
end

@testset "LU factorization" begin
    @testset "LU factorization 1" begin
        M = BandMatrix([[3,1]], [5,3,8], [[1,4]])
        l,u = lu(M)
        @test Matrix(l*u) == Matrix(M)
    end

    @testset "LU factorization 2" begin
        B = BandMatrix([[3.0, 9.0, 2.0]], [2.0, 3.0, 5.0, 3.0], [[6.0, 2.0, 4.0]])
        l,u = lu(B)
        @test Matrix(l*u) == Matrix(B)
    end
end
